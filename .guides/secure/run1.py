#!/usr/bin/env python

import os, requests, json

bashCommand = "free -hm"
import subprocess
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()
out = output[85:99]
print(out)

s = requests.Session()
s.mount('https://', requests.adapters.HTTPAdapter(max_retries=3))
r = s.get("{0}&grade={1}".format(os.environ['CODIO_AUTOGRADE_URL'], 27))
print r.content

parsed = json.loads(r.content)

exit( 0 if parsed['code'] == 1 else 1)